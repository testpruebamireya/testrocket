import { useState, useEffect } from 'react';

function App() {

  const [usuarios, setUsuarios] = useState([])
  const [recuperado, setRecuperado] = useState(false)


  const [datos, setDatos] = useState({
    id: '',
    nombre: '',
    segundonombre: '',
    apellidopaterno: '',
    apellidomaterno: '',
    fechanacimiento: '',
    email: '',
    telefono: '',
})


  function cambioNombre(e) {
    setDatos((valores) => ({
      ...valores,
      nombre: e.target.value,
    }))
  }

  function cambioSegundoNombre(e) {
    setDatos((valores) => ({
      ...valores,
      segundonombre: e.target.value,
    }))
  }

  function cambioApellidoPaterno(e) {
    setDatos((valores) => ({
      ...valores,
      apellidopaterno: e.target.value,
    }))
  }

  function cambioApellidoMaterno(e) {
    setDatos((valores) => ({
      ...valores,
      apellidomaterno: e.target.value,
    }))
  }


  function cambioFechaNacimiento(e) {
    setDatos((valores) => ({
      ...valores,
      fechanacimiento: e.target.value,
    }))
  }


  function cambioEmail(e) {
    setDatos((valores) => ({
      ...valores,
      email: e.target.value,
    }))
  }

  function cambioTelefono(e) {
    setDatos((valores) => ({
      ...valores,
      telefono: e.target.value,
    }))
  }



  function procesar(e) {
    e.preventDefault();
    e.target.reset();
    var datosEnviar={firstname:datos.nombre,middlename:datos.segundonombre,lastname:datos.apellidopaterno,lastname2: datos.apellidomaterno,datebirth:datos.fechanacimiento,email:datos.email,cellphone:datos.telefono}
    fetch('http://localhost/Mireyitaa/test/AltaRegistro.php',{
      method:"POST",
      body:JSON.stringify(datosEnviar)
    })
    alert('Dato cargado ' + datos.id + ' ' 
      +datos.nombre
      +datos.segundonombre
      +datos.apellidopaterno
      +datos.apellidomaterno
      +datos.fechanacimiento
      +datos.email
      +datos.telefono
      );
  }

  useEffect(() => {
    fetch('http://localhost/Mireyitaa/test/ObtenerRegistros.php')
      .then((response) => {
        return response.json()
      })
      .then((usuarios) => {
        setUsuarios(usuarios)
        setRecuperado(true)
      })
  }, [])


  return (
    <div>
      <form onSubmit={procesar}>
        <p>¿Cual es tu nombre ?</p>
        <p>Ingrese nombre:<input type="text" value={datos.nombre} onChange={cambioNombre} /></p>
        <p>Ingrese segundo nombre:<input type="text" value={datos.segundonombre} onChange={cambioSegundoNombre} /></p>
        <p>Ingrese apellido paterno:<input type="text" value={datos.apellidopaterno} onChange={cambioApellidoPaterno} /></p>
        <p>Ingrese apellido materno:<input type="text" value={datos.apellidomaterno} onChange={cambioApellidoMaterno} /></p>
        <p>Ingrese fecha de nacimiento:<input type="text" value={datos.fechanacimiento} onChange={cambioFechaNacimiento} /></p>
        <p>Datos de contacto</p>
        <p>Ingrese email:<input type="text" value={datos.email} onChange={cambioEmail} /></p>
        <p>Ingrese telefono:<input type="text" value={datos.telefono} onChange={cambioTelefono} /></p>
        
        <p><input type="submit" value="Iniciar" /></p>
      </form>
      <hr />
      <h3>Datos Ingresados</h3>
      <p style={{backgroundColor: "lightpink"}}>{datos.nombre} {datos.apellidopaterno}</p>
      <p style={{backgroundColor: "lightpink"}}>Fecha Nacimiento:{datos.fechanacimiento}</p>
      <p style={{backgroundColor: "lightpink"}}>Correo electronico:{datos.email}</p>
      <p style={{backgroundColor: "lightpink"}}>Telefono celular:{datos.telefono}</p>


      <table border="1">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Segundo Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Fecha Nacimiento</th>
              <th>Email</th>
              <th>Telefono</th>
            </tr>
          </thead>
          <tbody>
            {usuarios.map(usr => {
              return (
                <tr key={usr.id}>
                  <td>{usr.id}</td>
                  <td>{usr.first_name}</td>
                  <td>{usr.middle_name}</td>
                  <td>{usr.last_name}</td>
                  <td>{usr.last_name_2}</td>
                  <td>{usr.date_birth}</td>
                  <td>{usr.email}</td>
                  <td>{usr.cell_phone}</td>
                </tr>
              );
            })}
          </tbody>
        </table>


    </div>
  );
}

export default App;